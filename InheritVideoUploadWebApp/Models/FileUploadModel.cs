﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InheritVideoUploadWebApp.Models
{
    public class FileUploadModel
    {
        [DataType(DataType.Upload)]
        [Display(Name = "Upload Video")]
        [Required(ErrorMessage = "Please choose Video to upload.")]
        public string file { get; set; }
    }
}