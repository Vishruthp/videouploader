﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Collections.Specialized;

namespace InheritVideoUploadWebApp.Controllers
{
    public class FileUploadController : Controller
    {
        // GET: FileUpload
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> UploadFiles(HttpPostedFileBase file)
        {
            Boolean fileOK = false;
            if (ModelState.IsValid)
            {
                try
                {
                    if (file != null)
                    {
                        String fileExtension =System.IO.Path.GetExtension(file.FileName).ToLower();
                        String[] allowedExtensions ={".mp4", ".flv"};
                        for (int i = 0; i < allowedExtensions.Length; i++)
                        {
                            if (fileExtension == allowedExtensions[i])
                            {
                                fileOK = true;
                            }
                        }
                        if (fileOK)
                        {
                            string path = Path.Combine(Server.MapPath("~/UploadedFiles"), Path.GetFileName(file.FileName));
                            file.SaveAs(path);
                            MemoryStream st = new MemoryStream();
                            var stream = file.InputStream;
                            stream.CopyTo(st);
                            var bytearray = st.ToArray();
                            UploadMultipart(bytearray, file.FileName, file.ContentType, "http://localhost:12214/api/Files/Upload");
                        }
                        else
                        {
                            ViewBag.FileStatus = "Invalid file format.";
                            return View("Index");
                        }
                    }
                    ViewBag.FileStatus = "File uploaded successfully.";
                }
                catch (Exception ex)
                {

                    ViewBag.FileStatus = "Error while file uploading.";
                }

            }
            return View("Index");
        }
        public void UploadMultipart(byte[] file, string filename, string contentType, string url)
        {
            var webClient = new WebClient();
            string boundary = "------------------------" + DateTime.Now.Ticks.ToString("x");
            webClient.Headers.Add("Content-Type", "multipart/form-data; boundary=" + boundary);
            var fileData = webClient.Encoding.GetString(file);
            var package = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"file\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n{3}\r\n--{0}--\r\n", boundary, filename, contentType, fileData);

            var nfile = webClient.Encoding.GetBytes(package);
            //webClient.UploadProgressChanged
            
           var res = webClient.UploadData(new Uri(url), nfile);
        }
        private static string sendHttpRequest(string url, NameValueCollection values, NameValueCollection files = null)
        {
            string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
            // The first boundary
            byte[] boundaryBytes = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "\r\n");
            // The last boundary
            byte[] trailer = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "--\r\n");
            // The first time it itereates, we need to make sure it doesn't put too many new paragraphs down or it completely messes up poor webbrick
            byte[] boundaryBytesF = System.Text.Encoding.ASCII.GetBytes("--" + boundary + "\r\n");

            // Create the request and set parameters
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "multipart/form-data; boundary=" + boundary;
            request.Method = "POST";
            request.KeepAlive = true;
            request.Credentials = System.Net.CredentialCache.DefaultCredentials;

            // Get request stream
            Stream requestStream = request.GetRequestStream();

            foreach (string key in values.Keys)
            {
                // Write item to stream
                byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}", key, values[key]));
                requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                requestStream.Write(formItemBytes, 0, formItemBytes.Length);
            }

            if (files != null)
            {
                foreach (string key in files.Keys)
                {
                    if (System.IO.File.Exists(files[key]))
                    {
                        int bytesRead = 0;
                        byte[] buffer = new byte[2048];
                        byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: application/octet-stream\r\n\r\n", key, files[key]));
                        requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                        requestStream.Write(formItemBytes, 0, formItemBytes.Length);

                        using (FileStream fileStream = new FileStream(files[key], FileMode.Open, FileAccess.Read))
                        {
                            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                            {
                                // Write file content to stream, byte by byte
                                requestStream.Write(buffer, 0, bytesRead);
                            }

                            fileStream.Close();
                        }
                    }
                }
            }

            // Write trailer and close stream
            requestStream.Write(trailer, 0, trailer.Length);
            requestStream.Close();

            using (StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream()))
            {
                return reader.ReadToEnd();
            };
        }
        //[HttpPost]
        //public ActionResult UploadFiles(HttpPostedFileBase file)
        //{
        //    Boolean fileOK = false;
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            if (file != null)
        //            {
        //                String fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower();

        //                String[] allowedExtensions = { ".mp4", ".flv" };
        //                for (int i = 0; i < allowedExtensions.Length; i++)
        //                {
        //                    if (fileExtension == allowedExtensions[i])
        //                    {
        //                        fileOK = true;
        //                    }
        //                }
        //                if (fileOK)
        //                {
        //                    string path = Path.Combine(Server.MapPath("~/UploadedFiles"), Path.GetFileName(file.FileName));
        //                    file.SaveAs(path);
        //                }
        //                else
        //                {
        //                    ViewBag.FileStatus = "Invalid file format.";
        //                    return View("Index");
        //                }
        //            }
        //            ViewBag.FileStatus = "File uploaded successfully.";
        //        }
        //        catch (Exception)
        //        {

        //            ViewBag.FileStatus = "Error while file uploading.";
        //        }

        //    }
        //    return View("Index");
        //}


    }
}